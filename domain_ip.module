<?php

/**
 * @file
 * Enable ip restriction for each domain.
 */

/**
 * Implements hook_menu().
 */
function domain_ip_menu() {
  $items = array();
  $items['admin/structure/domain/view/%domain/ip'] = array(
    'title' => 'Ip',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'domain_ip_restriction_page',
    'page arguments' => array(4),
    'access arguments' => array('admin domain ip restriction'),
    'file' => 'domain_ip.admin.inc',
    'weight' => 45,
  );
  $items['admin/structure/domain/view/%domain/ip/message'] = array(
    'title' => '403 message for banned user',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('domain_ip_error_form'),
    'access arguments' => array('admin domain ip restriction'),
    'file' => 'domain_ip.admin.inc',
  );
  $items['admin/structure/domain/view/%domain/ip/delete/%'] = array(
    'title' => 'Delete IP range',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('domain_ip_delete', 4, 7),
    'access arguments' => array('admin domain ip restriction'),
    'file' => 'domain_ip.admin.inc',
  );
  $items['admin/structure/domain/view/%domain/ip/whitelist_own'] = array(
    'title' => 'Add your name for your ip',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('domain_ip_whitelist_own_form', 4),
    'access arguments' => array('admin domain ip restriction'),
    'file' => 'domain_ip.admin.inc',
  );
  $items['admin/structure/domain/view/%domain/ip/switchlist'] = array(
    'page callback' => 'drupal_get_form',
    'page arguments' => array('domain_ip_switch_list', 4),
    'access arguments' => array('admin domain ip restriction'),
    'file' => 'domain_ip.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function domain_ip_permission() {
  return array(
    'admin domain ip restriction' => array(
      'title' => t('Admin domain ip restriction'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_domain_delete().
 *
 * When domains are deleted from domain module, delete associated
 * records with domain-ip.
 */
function domain_ip_domain_delete($domain, $form_values = array()) {
  db_delete('domain_ip')
    ->condition('domain_id', $domain['domain_id'])
    ->execute();
  db_delete('domain_ip_list')
    ->condition('domain_id', $domain['domain_id'])
    ->execute();
}


/**
 * Implements hook_domain_bootstrap_lookup().
 *
 * Checks users ip against current list, i.e., whitelist or blacklist.
 *
 * @see domain_ip_get_ip_list()
 * @see domain_ip_check_ip()
 * @see domain_ip_deny_access()
 */
function domain_ip_domain_bootstrap_lookup($domain) {
  $domain_name = $domain['subdomain'];
  $domain_id = $domain['domain_id'];
  $list_type = domain_ip_get_domain_list($domain_id);
  $current_ip = ip_address();

  if ($list_type == 'Blacklist' || $list_type == NULL) {
    $blacklist = domain_ip_get_ip_list($domain_id, 'blacklist');
    foreach ($blacklist as $ip) {
      if (domain_ip_check_ip($ip->ip, $current_ip)) {
        domain_ip_deny_access($domain_name);
      }
    }
  }
  else {
    $whitelist = domain_ip_get_ip_list($domain_id, 'whitelist');
    foreach ($whitelist as $ip) {
      $whitelisted = 0;
      if (domain_ip_check_ip($ip->ip, $current_ip)) {
        $whitelisted = 1;
        break;
      }
    }
    if (!$whitelisted) {
      domain_ip_deny_access($domain_name);
    }
  }
}

/**
 * Handles denied users.
 *
 * @param string $domain_name
 *   Deny this domain name.
 *
 * Prints a message and exits if access is denied.
 */
function domain_ip_deny_access($domain_name) {
  header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden');
  $message = variable_get('Domain_IP_Banned_Error_Message', t('Sorry, you are banned for this site.'));
  print check_plain($message);
  exit();
}

/**
 * Checks users ip first against the whitelist, then the blacklist if needed.
 *
 * @param string $ip
 *   Black- or whitelisted ip-address.
 * @param string $current_ip
 *   Ip to be checked against the list, usually users current ip-address.
 *
 * @return boolean
 *   TRUE if the ip is on the list, FALSE if it is not.
 *
 * @see domain_ip_check_range()
 */
function domain_ip_check_ip($ip, $current_ip) {
  $type = strpos($ip, '-') ? 'range' : 'single';
  return ($type == 'single') ? $ip == $current_ip : domain_ip_check_range($ip, $current_ip);
}

/**
 * Checks if the given ip-address matches the given range.
 *
 * @param string $ip
 *   Black- or whitelisted ip-address range.
 * @param strng $current_ip
 *   Ip to be checked against the list, usually users current ip-address.
 *
 * @return boolean
 *   TRUE if the ip is on the list, FALSE if it is not.
 */
function domain_ip_check_range($ip, $current_ip) {
  $ip = explode('-', $ip);
  list($lower, $upper) = $ip;
  $lower_dec = (float) sprintf("%u", ip2long($lower));
  $upper_dec = (float) sprintf("%u", ip2long($upper));
  $ip_dec = (float) sprintf("%u", ip2long($current_ip));
  return (($ip_dec >= $lower_dec) && ($ip_dec <= $upper_dec));
}

/**
 * Creates/updates an entry to the ban database.
 *
 * @param int $domain_id
 *   Work with this domain.
 * @param string $ip
 *   IP-address or range to bw written to the database.
 * @param string $type
 *   Type of the list(blacklist, whitelist).
 * @param int $bid
 *   (optional) id of the current ban, used when existing record is updated.
 */
function domain_ip_write_record($domain_id, $ip, $type, $author, $bid = NULL) {
  db_merge('domain_ip')
    ->key(array('bid' => $bid))
    ->fields(array(
      'bid' => $bid,
      'domain_id' => $domain_id,
      'ip' => $ip,
      'type' => $type,
      'author' => $author,
    ))
    ->execute();
}

/**
 * Creates/updates an entry to the domain list type database.
 *
 * @param int $domain_id
 *   Work with this domain.
 * @param string $list
 *   Type of the list(blacklist, whitelist).
 */
function domain_ip_list_write_record($domain_id, $list) {
  db_merge('domain_ip_list')
    ->key(array('domain_id' => $domain_id))
    ->fields(array(
      'list' => $list,
    ))
    ->execute();
}

/**
 * Retrieves list of banned ip from the database.
 *
 * @param string $type
 *   (optional) Retrieve only the ip's of given list type(blacklist, whitelist).
 *
 * @return array
 *   An array of black-/whitelisted IP-addresses.
 */
function domain_ip_get_ip_list($domain_id, $type = '') {
  $query = db_select('domain_ip', 'list');
  if ($type) {
    $query->condition('list.domain_id', $domain_id, '=')
      ->condition('list.type', $type, '=')
      ->fields('list', array('ip'));
  }
  else {
    $query->condition('list.domain_id', $domain_id, '=')
      ->fields('list');
  }
  return $query->execute()->fetchAll();
}

/**
 * Retrieves list type of a specific domain from the database.
 *
 * @param int $domain_id
 *   Retrieve currently used list type(blacklist, whitelist) of domain
 *   with $domain_id.
 *
 * @return string
 *   List type of this domain will be returned.
 */
function domain_ip_get_domain_list($domain_id) {
  $query = db_select('domain_ip_list', 'domain');
  $query->condition('domain.domain_id', $domain_id)
    ->fields('domain', array('list'));
  return $query->execute()->fetchField();
}
