
Description
-----------
This module works with Domain module to provide ip restriction for each domain.
It's is a combination of Ip_ranges http://drupal.org/project/ip_ranges and
Domain Access http://drupal.org/project/domain.

Main differences from Ip_ranges are:
- Provide ip restriction/range for each domain.
- Only one ip list set(Whitelist or Blacklist) is used at one time.
- Provide ip owner field for added ip to make ip recognized easily.
- Customize message to restricted visitors.


Prerequisites
-------------
Domain_ip module requires Domain module to be installed.

Installation
------------
Install as usual, see http://drupal.org/node/70151 for further information.

Configuration
-------------
Configure user permissions in Administration > People > Permissions:
- Admin domain ip restriction

  Users in roles with the "Admin domain ip restriction" permission could access
  all domain ip setting pages.

Configure ip restriction for each domain:
Go to 'admin/structure/domain', edit one domain, and click IP tab in domain
editing page. Then you could add ip ranges to this specific domain.

Important
---------
By default, Domain_ip module uses Blacklist. Before switching to Whitelist,
please make sure you have clicked 'Click here to whitelist your own IP-address'
, or you will be banned from this domain immediately!
